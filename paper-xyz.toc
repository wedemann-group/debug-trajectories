{
  "paper": {
    "title": "Another file",
    "authors": [
      "Mario, Luigi"
    ],
    "doi": "test"
  },
  "files": [
    {
      "name": "my system 2",
      "path": "directory/Active_293.trj"
    },
    {
      "name": "A zip that contains two trajectories!",
      "path": "directory/Active_293.zip"
    }
  ]
}
