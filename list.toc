{
  "paper": {
    "title": "Differences in nanoscale organization of regulatory active and inactive human chromatin",
    "authors": [
      "Brandstetter, Katharina",
      "Zülske, Tilo",
      "Ragoczy, Tobias",
      "Hörl, David",
      "Guirao-Ortiz, Miguel",
      "Steinek, Clemens",
      "Barnes, Toby",
      "Stumberger, Gabriela",
      "Schwach, Jonathan",
      "Haugen, Eric",
      "Rynes, Eric",
      "Korber, Philipp",
      "Stamatoyannopoulos, John A.",
      "Leonhardt, Heinrich",
      "Wedemann, Gero",
      "Harz, Hartmann"
    ],
    "doi": "10.1016/j.bpj.2022.02.009"
  },
  "files": [
    {
      "name": "my system 1",
      "path": "directory/Active_293.trj"
    },
    {
      "name": "A zip that contains two trajectories!",
      "path": "directory/Active_293.zip"
    },
    {
      "name": "Trajectory with Cohesin",
      "path": "directory/cohesin.trj"
    },   
	{
      "name": "Fake PBC Trajectory",
      "path": "directory/confPBCTEST_short.trj"
    }
  ]
}
